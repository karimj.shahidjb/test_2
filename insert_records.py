from pymongo import MongoClient

# Connect to MongoDB running on localhost
client = MongoClient("mongodb://localhost:27017/")

# Create the database
db = client["demographics"]

# Create the collection
collection = db["citizens"]

# Sample data to be inserted
sample_data = [
    {
        "aadhaar_number": "123456789012",
        "name": "John Doe",
        "email": "john.doe@example.com",
        "phone_number": "+911234567890",
        "address": "123 Elm Street, Springfield"
    },
    {
        "aadhaar_number": "234567890123",
        "name": "Jane Smith",
        "email": "jane.smith@example.com",
        "phone_number": "+919876543210",
        "address": "456 Oak Avenue, Metropolis"
    },
    {
        "aadhaar_number": "345678901234",
        "name": "Robert Brown",
        "email": "robert.brown@example.com",
        "phone_number": "+918765432109",
        "address": "789 Pine Road, Gotham"
    }
]

# Insert sample data into the collection
collection.insert_many(sample_data)

# Retrieve and print the inserted data to verify
for citizen in collection.find():
    print(citizen)
